import 'package:appwrite/models.dart';
import 'package:flutter/material.dart';
import 'package:appwrite/appwrite.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  Client client = Client();
  client
      .setEndpoint('https://cloud.appwrite.io/v1')
      .setProject('655116463da96d6162ed');

  Account account = Account(client);

  runApp(MyApp(account: account));
}

class MyApp extends StatefulWidget {
  final Account account;

  const MyApp({required this.account, super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: MyHomePage(title: 'Appwrite Flutter Demo', account: widget.account),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key, required this.title, required this.account});

  final String title;
  final Account account;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  User? loggedInUser;

  @override
  void initState() {
    super.initState();
    syncUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _getButton(),
          ],
        ),
      ),
    );
  }

  Widget _getButton() {
    if (loggedInUser == null) {
      return ElevatedButton(
          onPressed: () async {
            await widget.account.createOAuth2Session(provider: "google");
            syncUser();
          },
          child: Text("Login with Google"));
    }

    return ElevatedButton(
        onPressed: () async {
          await widget.account.deleteSession(sessionId: "current");
        },
        child: Text("Logout"));
  }

  void syncUser() {
    widget.account.get().then((user) {
      setState(() {
        loggedInUser = user;
      });
    }).onError((error, stackTrace) {
      print("User is not logged in ");
    });
  }
}
